import React, { PureComponent } from 'react';
import styles from './FormStyles.scss';
import Label from '../../../generic/Label/Label';
import TextField from '../../../generic/TextField/TextField';
import Button from '../../../generic/Button/Button';
import Link from 'react-router-dom/es/Link';
import PropTypes from 'prop-types';

export default class MarketForm extends PureComponent {

  static propTypes = {
    toggleMarketModal: PropTypes.func.isRequired,
    goToMarket: PropTypes.func.isRequired,
    createMarket: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      address: '0xc562862d7b6074d572f49bfdcd44a25bc4d31634',
    };
  }

  onAddressChange = (label, inputState) => {
    this.setState({ [label]: inputState.value });
  };

  goToMarket = () => {
    const { address } = this.state;
    if (address.replace(/\s+/g, '')) this.props.goToMarket(address);
  };

  render() {
    return (
      <div className={styles.container}>
        <Label className={styles.header} text='Welcome'/>
        <div>
          <Label className={styles.label} text='Go to existing market:'/>
          <div className={styles.row}>
            <TextField onChange={this.onAddressChange}
                       label='address'
                       placeholder='Contact address'
                       value={this.state.address}
                       className={styles.field}/>
            <Button className={styles.button} onClick={this.goToMarket}>Go</Button>
          </div>
        </div>
        <span className={styles.text}>or</span>
        <Button className={styles.newMarket} onClick={this.props.createMarket}>
          <Link className={styles.link} to='/market'>Create new market</Link>
        </Button>
        <span className={styles.signInLink}>
          Want to create new account &rarr; <span onClick={this.props.toggleMarketModal}>Create account</span>
        </span>
      </div>);
  }
}
