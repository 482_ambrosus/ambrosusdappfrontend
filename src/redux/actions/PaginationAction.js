export function setPaginationPage(paginationPage) {
  return {
    type: 'SET_PAGINATION_PAGE',
    paginationPage,
  };
}
