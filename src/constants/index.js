export const MAX_TRADES_AMOUNT = Math.floor((window.innerWidth - 200) / 270) * 3;
export const MAX_OFFERS_AMOUNT = MAX_TRADES_AMOUNT;
export const CONTRACT_ADDRESS = '0x989510249253eb5d6a19df920795700a1b6f2278';        // kovan
